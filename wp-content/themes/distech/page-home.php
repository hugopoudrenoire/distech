<?php
/**
 * Template Name: Page Home
 */
?>

<?php while (have_posts()) : the_post(); ?>


    <section id="page-home" class="fit-screen">
        <div class="container-fluid">
            <div class="row">
                <div id="carousel-home" class="carousel slide fit-screen" data-ride="carousel">
                    <!-- Indicators -->
                    <ol class="carousel-indicators">
                        <li data-target="#carousel-home" data-slide-to="0" class="active"></li>
                        <li data-target="#carousel-home" data-slide-to="1"></li>
                        <!-- <li data-target="#carousel-home" data-slide-to="2"></li> -->
                    </ol>


                    <!-- Wrapper for slides -->
                    <div class="carousel-inner fit-screen" role="listbox">
                        <div class="item fit-screen slide-1 active" style="background-image: url('<?php echo get_template_directory_uri(); ?>/assets/images/home/slide-2.jpg');">
                            <div class="outerCenter">
                                <div class="middleCenter">
                                    <div class="innerCenter">
                                        <h1>What’s New!</h1>
                                        <p class="info">Take an interactive tour of our AHR booth and discover our latest innovations!</p>
                                        <a href="<?php echo pn_get_url_from_template("page-map.php"); ?>" class="btn-green">Take a tour</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="item fit-screen slide-2" style="background-image: url('<?php echo get_template_directory_uri(); ?>/assets/images/home/slide-1.jpg');">
                            <div class="outerCenter">
                                <div class="middleCenter">
                                    <div class="innerCenter">
                                        <h1>Connect with Us!</h1>
                                        <p class="info">Be the first to know about our latest innovations, events and more!</p>
                                        <a href="<?php echo pn_get_url_from_template("page-connect.php"); ?>" class="btn-green">Connect</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- <div class="item fit-screen slide-3" style="background-image: url('<?php echo get_template_directory_uri(); ?>/assets/images/home/slide-3.jpg');">
                            <div class="outerCenter">
                                <div class="middleCenter">
                                    <div class="innerCenter">
                                        <h1>It's Time You Specified ECLYPSE!</h1>
                                        <p class="info">Gain access to Distech Controls’ easy-to-use, time-saving specification builder, and win! Top prizes include $100 Amazon gift cards!</p>
                                        <a href="<?php echo pn_get_url_from_template("page-specified.php"); ?>" class="btn-green">Win instant prizes</a>
                                    </div>
                                </div>
                            </div>
                        </div> -->
                    </div>
                </div>
            </div>
        </div>
    </section>

<?php endwhile; ?>
