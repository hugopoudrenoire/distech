<?php
/**
 * Template Name: Page Products
 */
?>


<?php

$produits = $pnPostTypeProduit->getAllPosts();
//print_r($produits);
?>

<?php while (have_posts()) : the_post(); ?>
    <section id="page-products" class="wrapper-content">
        <div class="container-fluid">
            <div class="row">
				
				<div id="nav">
					<div class="back">
						<div class="outerCenter">
							<div class="middleCenter">
								<div class="innerCenter">
									<a href="<?php echo pn_get_url_from_template("page-map.php"); ?>">Back to map</a>
								</div>
							</div>
						</div>
					</div>
					<!-- changer la COULEUR ET le NOM de la class selon la category -->
					<div class="category corail">
						<div class="outerCenter">
							<div class="middleCenter">
								<div class="innerCenter">
									<a href="">IP-Based Mechanical Equipment Control</a>
								</div>
							</div>
						</div>
					</div>
				</div>
                
                <div id="carousel-product" class="carousel slide" data-ride="carousel" data-interval="false">
                    <div class="carousel-inner" role="listbox">
                        <?php
                        foreach($produits as $key=>$produit){
                            $cat = wp_get_post_terms($produit->ID, "categorieproduit");
                            foreach($cat as $c){
                                printf("<a href='%s'>link</a>", get_term_link( $c )); ;
                            }

                        ?>
                        <div data-id="<?php echo $produit->ID; ?>" class="item <?php if($key==0) echo "active"; ?>">
                            <div id="product-picture">
                                <!-- style="background-image: url('<?php echo get_template_directory_uri(); ?>/assets/images/new/produit-big.jpg');" -->
                                <img src="<?php echo $produit->image; ?>" alt="">
                            </div>

                            <div id="product-title" class="col-sm-12">
                                <p class="title"><?php echo $produit->title; ?></p>
                            </div>                        
                        </div>
                        <?php
                        }                        
                        ?>                                                             
                    </div>                                                           

                    <div id="controls" class="col-sm-12">
                        <p id="read-more" class="btn-green">read more</p>
                        <div class="arrows">
                            <img  href="#carousel-product" role="button" data-slide="prev" src="<?php echo get_template_directory_uri(); ?>/assets/images/ui/btn-arrow-prev.svg" alt="">
                            <img href="#carousel-product" role="button" data-slide="next" src="<?php echo get_template_directory_uri(); ?>/assets/images/ui/btn-arrow-next.svg" alt="">
                        </div>
                    </div>  
                </div>                
                
                

				<!-- 
				***************************************
				ICI C'EST CACHÉ & SI ON CLIQUE SUR PLUS CA AFFICHE
				***************************************
				-->
				<div id="product-content">
					<div id="product-infos" class="col-sm-12">
						<p>The ECLYPSE Connected System Controller is a modular and scalable platform, allowing you to cost effectively address any HVAC application.</p>
						<p>Consisting of a control automation and connectivity server, power supply, and I/O modules, select the type and quantity of modules to cost effectively meet your exacting requirements.</p>
					</div>

					<div id="product-video">
						<iframe src="https://player.vimeo.com/video/116912711?title=0&byline=0&portrait=0" width="100%" height="280" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>
					</div>

					<div id="buzz" class="col-sm-12">
						<p class="title">What’s the buzz all about?</p>
						<blockquote>
							<p>Additional power supply, control, automation and connectivity servers, and I/O modules, allowing you to cost effectively address a wider range of applications.</p>
						</blockquote>
					</div>

					<div id="benefit" class="col-sm-12">
						<p class="title">Benefits</p>
						<ul>
							<li>A scaled aproach to cost effectively address project requirements</li>
							<li>Empowere IP connectivity</li>
							<li>Connect from anywhere using your mobile device</li>
						</ul>
						<div class="wrapper-btn">
							<a href="" class="btn-green">Get the specs & win</a>
						</div>
					</div>

					<div id="awards" class="col-sm-12">
						<p class="title">Awards</p>
						<ul>
							<li><img src="<?php echo get_template_directory_uri(); ?>/assets/images/new/award-1.jpg" alt=""></li>
							<li><img src="<?php echo get_template_directory_uri(); ?>/assets/images/new/award-2.jpg" alt=""></li>
							<li><img src="<?php echo get_template_directory_uri(); ?>/assets/images/new/award-3.jpg" alt=""></li>
							<li><img src="<?php echo get_template_directory_uri(); ?>/assets/images/new/award-4.jpg" alt=""></li>
						</ul>
					</div>

					<div id="features" class="col-sm-12">
						<p class="title">Features</p>
						<ul>
							<li>BTL listed as BACnet Building Controllers (B-BC)</li>
							<li>Simultaneous support of wired IP and Wi-Fi connectivity on a same controller</li>
							<li>Support Wi-Fi mesh </li>
							<li>Enables BACnet MS/TP and Modbus routing</li>
							<li>Proven high performance: accelerated processing, reading and control of up to 320 points</li>
							<li>Advanced, built-in security features and authentication service</li>
							<li>Hot-swappable tool-less design and unique latching mechanism</li>
						</ul>
					</div>

					<div id="related" class="col-sm-12">
						<p class="title">Software and Tools</p>
						<ul>
							<li>
								<a href="">
									<img src="<?php echo get_template_directory_uri(); ?>/assets/images/new/produit-thumb-1.jpg" alt="">
									<p class="name">Allure EC-Smart-Air</p>
								</a>
							</li>
							<li>
								<a href="">
									<img src="<?php echo get_template_directory_uri(); ?>/assets/images/new/produit-thumb.jpg" alt="">
									<p class="name">Allure EC-Smart-Air</p>
								</a>
							</li>
							<li>
								<a href="">
									<img src="<?php echo get_template_directory_uri(); ?>/assets/images/new/produit-thumb.jpg" alt="">
									<p class="name">Allure EC-Smart-Air</p>
								</a>
							</li>
							<li>
								<a href="">
									<img src="<?php echo get_template_directory_uri(); ?>/assets/images/new/produit-thumb-1.jpg" alt="">
									<p class="name">Allure EC-Smart-Air</p>
								</a>
							</li>
						</ul>
					</div>
					<div class="wrapper-btn-up col-sm-12">
						<img id="btn-up" src="<?php echo get_template_directory_uri(); ?>/assets/images/ui/btn-arrow-up.svg" alt="">
					</div>
				</div>

            </div>
        </div>
    </section>
    <script type="text/javascript">
        var ajaxurl = '<?php echo admin_url('admin-ajax.php'); ?>';
    </script>
<?php endwhile; ?>
