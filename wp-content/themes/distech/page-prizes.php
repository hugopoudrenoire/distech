<?php
/**
 * Template Name: Page Prizes
 */
?>

<?php while (have_posts()) : the_post(); ?>
	<section id="page-prizes" class="wrapper-content">
	    <div class="container-fluid">
	        <div class="row">
	            <div class="col-sm-12 section section-1">
	                <h1>Congradulations!</h1>
	                <p>Prize: You're the winner of <span class="title-price">ECLYPSE water bottle and sunglasses</span> and <span class="title-price">$100 Amazon gift card</span> !</p>
	            </div>

	            <div class="col-sm-12 section section-2">
	                <img src="<?php echo get_template_directory_uri(); ?>/assets/images/prizes/price-1.png" alt="">
	                <img src="<?php echo get_template_directory_uri(); ?>/assets/images/prizes/price-2.png" alt="">
	            </div>

	            <div class="col-sm-12 section section-3">
	                <p class="sub-title">Claim your prize by presenting this message to the Distech Controls hostess, stationed by our water bar.</p>
					<a href="" class="btn-green">share the spec builder</a>
					<a href="" class="btn-green">what's new - take a tour</a>
	            </div>
	        </div>
	    </div>
	</section>
<?php endwhile; ?>
