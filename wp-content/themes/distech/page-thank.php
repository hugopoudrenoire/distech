<?php
/**
 * Template Name: Page Thank You
 */
?>

<?php while (have_posts()) : the_post(); ?>
	<section id="page-thank" class="wrapper-content">
	    <div class="container-fluid">
	        <div class="row">

	             <div id="section-1" class="col-sm-12 section">
	             	<div class="outerCenter">
	            		<div class="middleCenter">
	            			<div class="innerCenter">
				                <h1>Thank You</h1>
				                <p>How would you rate your experience at the Distech Controls booth?</p>
			                    <section id="sectionVote">
			                        <input id="input-star" type="number" class="rating" min=0 max=5 step=1 data-size="sm" >

			                        <button id="btnSubmitThank" class="btn-green">Submit</button>                        
			                        
			                    </section>
			                    <section id="sectionVoted" style="display:none;">
			                        <p>Voted !</p>
			                    </section>  
			                </div>
	            		</div>
	            	</div>                  

	            </div>
	        </div>
	    </div>
	</section>

<?php endwhile; ?>
