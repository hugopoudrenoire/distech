<?php
/**
 * Template Name: Page Connect
 */
?>

<?php while (have_posts()) : the_post(); ?>


    <section id="page-connect" class="wrapper-content">
        <div class="container-fluid">
            <div class="row">
                <div id="section-1" class="col-sm-12 col-md-6 section wow fadeInDown" data-wow-delay="0.2s">
                    <div class="outerCenter">
                        <div class="middleCenter">
                            <div class="innerCenter">
                                <h1 class="wow fadeInDown">Subscribe to our<br>Bulletin</h1>
                                <p>We would love to share our news, tip and tricks with you. Sign up for our quarterly bulletins, developed specifically with you&nbsp;in&nbsp;mind!</p>
                                <!-- Begin MailChimp Signup Form -->
                                <form id="formCourriel">
                                    <input type="hidden" value="add_courriel" name="action">                                    
                                    <input type="email" value="" name="courriel" class="email" id="mce-EMAIL" placeholder="Email" required>
                                   <!-- real people should not fill this in and expect good things - do not remove this or risk form bot signups-->
                                    <p id="txtSubscribe" style="display: none; margin-bottom: 0px">Thank you!</p>
                                   <div class="clear"><input type="submit" value="Subscribe" name="subscribe" id="mc-embedded-subscribe" class="button"></div>
                                </form>
                                <!--End mc_embed_signup--> 
                            </div>
                        </div>
                    </div>
                </div>

                <div id="section-2" class="col-sm-12 col-md-6 section wow fadeInDown" data-wow-delay="0.2s">
                    <div class="outerCenter">
                        <div class="middleCenter">
                            <div class="innerCenter">
                                <h1 class="wow fadeInDown">Follow us on<br>LinkedIn</h1>
                                <p>Want to stay up to date in real-time? Then follow us on LinkedIn, where we share all of our&nbsp;latest&nbsp;news!</p>
                                <script src="//platform.linkedin.com/in.js" type="text/javascript"> lang: en_US</script>
                                <script type="IN/FollowCompany" data-id="1729129" data-counter="top"></script>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </section>

<?php endwhile; ?>
