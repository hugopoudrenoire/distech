/* ========================================================================
 * DOM-based Routing
 * Based on http://goo.gl/EUTi53 by Paul Irish
 *
 * Only fires on body classes that match. If a body class contains a dash,
 * replace the dash with an underscore when adding it to the object below.
 *
 * .noConflict()
 * The routing is enclosed within an anonymous function so that you can
 * always reference jQuery with $, even when in .noConflict() mode.
 * ======================================================================== */
var totalClick=0;
function getCookie(name)
{
  var re = new RegExp(name + "=([^;]+)");
  var value = re.exec(document.cookie);
  return (value != null) ? unescape(value[1]) : null; 
}

function onPlay(){
  console.log("cacncer");
}


(function($) {

  // Use this variable to set up the common and page specific functions. If you
  // rename this variable, you will also need to rename the namespace below.
  var Sage = {
    // All pages
    'common': {
      init: function() {
        
        //DATA TRACKING
        $('[data-tracking]').click(function(){
          //CATEGORY | EVENT | LABEL
          var data = $(this).attr('data-tracking').split('|');
          ga('send', 'event', data[0], data[1], data[2]);
        });        
        
        $(window).load(function(){
            $('#wrapper-spinner').fadeOut(500);
        });

        $("#input-id").rating();
        
        //********
        //MENU
        //********
        $('#burger-menu').click(function(){
          if($('#main-menu').hasClass('open')) {
            $('#main-menu').animate({
              'left': '-100%'
            }, 400, function(){
              $(this).removeClass('open');
              $('#burger-menu').removeClass('close-menu');
            });
          } else {
            $('#main-menu').animate({
              'left': '0'
            }, 400, function(){
              $(this).addClass('open');
              $('#burger-menu').addClass('close-menu');
            });
          }

        });

        $("#main-menu").swipeleft(function() {  
            $('#main-menu').animate({
              'left': '-100%'
            }, 400, function(){
              $(this).removeClass('open');
              $('#burger-menu').removeClass('close-menu');
            });
        }); 

        //********
        //BTN ON ET OFF
        //********
        $("[bootstrap-checkbox]").bootstrapSwitch({
          'state': true,
          'onText': '<img class="check-white" src="' + templateDirectory + '/assets/images/ui/check-white.svg">',
          'offText': '<img class="x-white" src="' + templateDirectory + '/assets/images/ui/x-white.svg">',
          'labelText': '|||'
        });

        $("[bootstrap-checkbox]").bootstrapSwitch({
          'state': true,
          'onText': 'OUI',
          'offText': 'NON',
          'labelText': '|||'
        });

        //********
        //SCROLL TOP 
        //********
        $('#btn-up').click(function(){
          $("html, body").animate({ scrollTop: "0px" });
        });

        //********
        //INITIATION DU WOW
        //********
        var wow = new WOW(
          {
            boxClass:     'wow',      // animated element css class (default is wow)
            animateClass: 'animated', // animation css class (default is animated)
            offset:       0,          // distance to the element when triggering the animation (default is 0)
            mobile:       true,       // trigger animations on mobile devices (default is true)
            live:         true,       // act on asynchronously loaded content (default is true)
            callback:     function(box) {
              // the callback is fired every time an animation is started
              // the argument that is passed in is the DOM node being animated
            },
            scrollContainer: null // optional scroll container selector, otherwise use window
          }
        );
        wow.init();
        Sage.page_template_page_thank_php.init();
      },
      finalize: function() {
        // JavaScript to be fired on all pages, after page specific JS is fired
      }
    },
    // Home page
    'home': {
      init: function() {
        // JavaScript to be fired on the home page
      },
      finalize: function() {
        // JavaScript to be fired on the home page, after the init JS
      }
    },
    // About us page, note the change from about-us to about_us.
    'about_us': {
      init: function() {
        // JavaScript to be fired on the about us page
      }
    },
    // About us page, note the change from about-us to about_us.
    'page_template_page_home': {
      init: function() {
              var windowHeight = $(window).height();
              var windowWidth = $(window).width();

              $(window).resize(function() {
                  var windowHeight = $(window).height();
                  var windowWidth = $(window).width();

                  // if(windowWidth < 768) {
                    $('.fit-screen').height(windowHeight);
                  // } 
              });

              $(window).load(function(){
                  $(window).trigger('resize');

                  $('#page-home h1').animate(
                      {
                        'top': '0px',
                        'opacity': 1
                      },
                      {
                          duration: 300,
                          easing: 'easeOutExpo',
                      }
                  );

                  $('#page-home  p').delay( 200 ).animate(
                      {
                        'top': '0px',
                        'opacity': 1
                      },
                      {
                          duration: 300,
                          easing: 'easeOutExpo',
                      }
                  );

                  $('#page-home .btn-green').delay( 400 ).animate(
                      {
                        'top': '0px',
                        'opacity': 1
                      },
                      {
                          duration: 600,
                          easing: 'easeOutElastic'
                      }
                  );

              }); 

              $("#carousel-home").swiperight(function() {  
                  $(this).carousel('prev');  
              });  
              $("#carousel-home").swipeleft(function() {  
                  $(this).carousel('next');  
              }); 

              //changer dynamiquement le titre d ela banniÃ¨re
              $('#carousel-home').on('slide.bs.carousel', function (e) {
                //$('h1, p, .btn-green').fadeOut(200);
                $('#page-home h1, #page-home  p, #page-home  .btn-green').animate(
                    {
                      
                      'opacity': 0,
                      'top': '-30px'
                    },
                    {
                        duration: 1000,
                        easing: 'easeOutExpo',
                    }
                );


              });

              //changer dynamiquement le titre d ela banniÃ¨re
              $('#carousel-home').on('slid.bs.carousel', function (e) {

                $('#page-home h1, #page-home  p, #page-home  .btn-green').clearQueue();
                $('#page-home h1, #page-home  p, #page-home  .btn-green').stop();
                
                $('#page-home  h1').animate(
                    {
                      'top': '0px',
                      'opacity': 1
                    },
                    {
                        duration: 300,
                        easing: 'easeOutExpo',
                    }
                );

                $('#page-home  p').delay( 200 ).animate(
                    {
                      'top': '0px',
                      'opacity': 1
                    },
                    {
                        duration: 300,
                        easing: 'easeOutExpo',
                    }
                );

                $('#page-home  .btn-green').delay( 400 ).animate(
                    {
                      'top': '0px',
                      'opacity': 1
                    },
                    {
                        duration: 600,
                        easing: 'easeOutElastic'
                    }
                );

                //$('h1').fadeIn(200);
                // $('p').fadeIn(400);
                // $('.btn-green').fadeIn(600);
              });
            }
    },
    'page_template_page_products' :{
      init:function(){

        //Tracking play vimeo
        var haveClickVimeo = 0;        
        
        //**************
        
        var animateProgressBar = function () {
            $('.bar').animate({
              'width': '100%'
            }, 10000, function(){
              $('#carousel-product').carousel('next');
              $('.bar').css('width','0');
            });
        };

        animateProgressBar();

        //******************
        
        //Load content via ajax
        $('#read-more').click(function(e, param){
          var id = $("#carousel-product .item.active").data("id");
          $('#wrapper-spinner').fadeIn(100);

          $('.bar').clearQueue();
          $('.bar').stop().css('width','0');

          $.ajax({
            url:ajaxurl,
            method: "POST",
            data: {id:id, action:"produit_read_more"}
          }).done(function (data){
            $("#product-content").html(data);        
            $('#wrapper-spinner').fadeOut(100);   
            $('#product-content').fadeIn(200, function(){
              if(param === undefined){
                $('html, body').animate({scrollTop: $('#product-content').offset().top - 80}, 'slow');                
              }
            });     
            
            //Tracking play vimeo
            haveClickVimeo = 0;        
            $('#product-video iframe').iframeTracker(false);            
            $('#product-video iframe').iframeTracker({
                blurCallback: function(){
                  if(haveClickVimeo === 0){
                    var nomVideo = $('#iframeVimeo').data("title");
                    ga('send', 'event', "Video", "Play", nomVideo);    
                  }
 
                  haveClickVimeo += 1;                  
                }
            });    
          });          

          $('#btn-up').fadeIn(200);
        });

        //********
        //CAROUSEL
        //********
        $("#carousel-product").swiperight(function() {  
            $(this).carousel('prev');
        });  
        $("#carousel-product").swipeleft(function() {  
            $(this).carousel('next');  
        });
        //On slide change remove contente
        $('#carousel-product').on('slide.bs.carousel', function () {
            $("#product-content").html("");  
            $('#btn-up').fadeOut(200);

            $('.bar').clearQueue();
            $('.bar').stop().css('width','0');  
        });

        $('#carousel-product').on('slid.bs.carousel', function () {
            animateProgressBar();
            //POUR LE GOOGLE TRACKING
            var title = $("#carousel-product .item.active").data("title");
            $("#read-more").attr("data-tracking", "Product|Read More|"+title);
        });  
        

      }
    },
    'page_template_page_map' :{
      init:function(){
        var height = $('#section').outerHeight();
        var height2 = $('#picture').outerHeight();

        // au load on cache le pop-up
        $('#pop-up').css('bottom', '-'+(height+height2)+'px');

        //clique sur le point
        $('.point').not('.question').click(function(){
          var categoryColor = $(this).data('categoryColor');
          var categoryTitle = $(this).data('categoryTitle');
          var categoryImage = $(this).data('image');
          var categoryLink = $(this).data('link');
          
          //Show/hide picture
          if(categoryImage === ""){
            $("#picture").hide();
          }else{
            $("#picture").show();  
            $("#picture").css("background-image", "url("+categoryImage+")");  //            
          }
          
          //Change tax link
          $("#pop-up-link").attr("href", categoryLink);

          $('#pop-up').animate({
              'bottom': '0px'
          }, 400);

          $('#section').removeAttr('class');
          $('#section').addClass(categoryColor);
          
          //$('#section #title').html("<p class='category-name'>" + categoryTitle + "</p>" + "<a class='category-link' href='"+ categoryLink +"'>" + "</a>");
          //Attribuer le lien et le titre de la catégorie
          $('#section #title').html("<p class='category-name'>" + categoryTitle + "</p>");
          $('.category-link').attr('href', categoryLink);
        });

        //clique sur le cancel
        $('.cancel').click(function(){
          var newHeight = $('#section').outerHeight();
          var newHeight2 = $('#picture').outerHeight();

          $('#pop-up').animate({
              'bottom': '-'+(newHeight+newHeight2)+'px'
          }, 400);
        });

        //clique sur les autres points
        $('.question').click(function(){
            if($('.txt-question').hasClass('open')) {
                $('.txt-question').fadeOut(500);
                $('.txt-question').removeClass('open');
            } else {
              $('.txt-question').fadeIn(500);
              setTimeout(function(){
                $('.txt-question').fadeOut(500);
                $('.txt-question').removeClass('open');
              }, 2000);
              $('.txt-question').addClass('open');
            }
        });

        $('.info').click(function(){
            if($('.txt-info').hasClass('open')) {
                $('.txt-info').fadeOut(500);
                $('.txt-info').removeClass('open');
            } else {
              $('.txt-info').fadeIn(500);
              setTimeout(function(){
                $('.txt-info').fadeOut(500);
                $('.txt-info').removeClass('open');
              }, 2000);
              $('.txt-info').addClass('open');
            }
        });

      }
    },
    'tax_categorieproduit' :{
      init:function(){
        Sage.page_template_page_products.init();
      }
    },
    'single_produit' :{
      init:function(){
        Sage.tax_categorieproduit.init();      
        $("#read-more").trigger("click", "noscroll");
      }
    },
    'page_template_page_connect' :{
      init:function(){
        //ipad
        $(window).resize(function(){
            var windowWidth = $(window).width();
            var windowHeight = $(window).height();

            if(windowWidth >= 992 && windowWidth < 1200) {
              $('#section-1').height(windowHeight);
              $('#section-2').height(windowHeight);

              $('#section-2 .innerCenter').height($('#section-1 .innerCenter').height());
            }

            if(windowWidth >= 1200) {
              $('#section-1').height(windowHeight);
              $('#section-2').height(windowHeight);

              $('#section-2 .innerCenter').height($('#section-1 .innerCenter').height());
            }

            if(windowWidth < 992) {
              $('#section-1').css('height', 'auto');
              $('#section-1 .innerCenter').css('height', 'auto');
              $('#section-2').css('height', 'auto');
              $('#section-2 .innerCenter').css('height', 'auto');
            }
            
        });

        $(window).trigger('resize');
        
        $("#formCourriel").submit(function(e){
          e.preventDefault();
          var formData = $(this).serialize();
          $.ajax({
            url:ajaxurl,
            method:"POST",
            data:formData
          }).success(function(data){
            if(data){
              console.log(data);
              $("#txtSubscribe").slideDown();   
              $("#formCourriel input[type='submit']").slideUp();              
              
            }
          });
        });
      }
    },
    'page_template_page_specified':{
      init:function(){
        //ipad
        $(window).resize(function(){
            var windowWidth = $(window).width();
            var windowHeight = $(window).height();

            if(windowWidth >= 992 && windowWidth < 1200) {
              $('#section-1').height(windowHeight - 60);
              $('#section-2').height(windowHeight - 60);
            }

            if(windowWidth >= 1200) {
              $('#section-1').height(windowHeight - 80);
              $('#section-2').height(windowHeight - 80);
            }

            if(windowWidth < 992) {
              $('#section-1').css('height', 'auto');
              $('#section-2').css('height', 'auto');
            }
            
        });

        $(window).trigger('resize');

        //Show/Hide sec depend on state
        $("#checkEngineer").on('switchChange.bootstrapSwitch', function(){
          if($(this).prop("checked")){
            $("#zoneEngineer").hide();
            $("#codeLinkedin").show();            
          }else{
            $("#zoneEngineer").show();   
            $("#codeLinkedin").hide();            
          } 
        });
        
        $("#modalSmartClick").click(function(e){
          e.preventDefault();
           $("#modalSmart").modal("show");
        });

        $("#modalLunchClick").click(function(e){
          e.preventDefault();
           $("#modalLunch").modal("show");
        });

        $("#modalBulletinClick").click(function(e){
          e.preventDefault();
           $("#modalBulletin").modal("show");
        });
        
        $("#formConcours").submit(function(e){
          e.preventDefault();
          $(".txtError").hide();
          
          
          //Valide les champs
          var error = 0;          
          $("#formConcours [data-required]").each(function(){
            $(this).removeClass("data-required");            
            if($(this).val() === ""){
              error=1;
              $(this).addClass("data-required");
            }
          });
          
          //Check if error
          if(error) {
            $("#txtMissingFields").slideDown();
          }
          //Si pas error
          else{
            $("#wrapper-spinner").show();        
            var sData = ($(this).serialize());
            //Call ajax
            $.ajax({
              url:ajaxurl,
              method:"POST",
              data:sData,
              dataType:"json"
            }).done(function(data){
              $("#wrapper-spinner").hide();                      
          
              //Si gagnant
              if((data.produit)){
                var txtGagner = "and <span class='title-price'>" + data.produit.title + "</span>";
                $("#txtPrix2").html(txtGagner).show();
                $("#imgPrix2").attr("src", data.produit.image).show();
              }
              
              if(data.error ){                
                $("#txtBadEmail").slideDown();                
              }else{
                $("#page-prizes").show();
                $("#page-specified").hide();
                $("#formConcours input[type=text]").val("");    
                $("html, body").animate({
                    scrollTop: 0
                }, 600);                     
              }
              
         
            });
          }
          
        });
      }
    },
    'page_template_page_thank_php':{
      init:function(){
        //GEST COOKIE
        var now = new Date();
        var time = now.getTime();
        time += 1000 * 60 * 5;
        now.setTime(time);        
        
        totalClick = getCookie("clic_c");
        if(totalClick === null){   
          document.cookie="clic_c=0 ; expires="+now.toUTCString() + ";path=/";       
          document.cookie="clic_tim="+now.toUTCString() + " ; expires="+now.toUTCString() + ";path=/";            
          
          totalClick = 0;          
        }
        
        //CPT Click
        $("body").click(function(e){
          totalClick++;
          var expire = now.toUTCString();
          expireCke = getCookie("clic_tim");
          if(expireCke !== null){  
            expire = expireCke;
          }          
          document.cookie="clic_c="+totalClick+" ;expires="+expire + ";path=/";         
          if(totalClick === 10){
            e.preventDefault();
            e.stopPropagation();
            $('#modal-thank').modal('show');
            $("#page-thank").show();
          }
        });        
        
        //SUBMIT THE VOTING
        $("#page-thank #btnSubmitThank").click(function(){
          $("#wrapper-spinner").show();
          var note = jQuery("#input-star").val();
          var sData = {action:"add_note", note:note};
          $.ajax({
            url:ajaxurl,
            method: "POST",
            data:sData,
          }).done(function(data){
            $("#sectionVoted").show();
            $("#sectionVote").hide();
            $("#wrapper-spinner").hide();
            $("#modal-thank").modal('hide');
            $("#page-thank").hide();
          });
        });

        //ipad
        $(window).resize(function(){
            var windowWidth = $(window).width();
            var windowHeight = $(window).height();

            if(windowWidth >= 768) {
              $('#section-1').height(windowHeight - 60);
            }

            if(windowWidth < 768) {
              $('#section-1').css('height', 'auto');
            }
            
        });

        $(window).trigger('resize');

      }
    }
  };

  // The routing fires all common scripts, followed by the page specific scripts.
  // Add additional events for more control over timing e.g. a finalize event
  var UTIL = {
    fire: function(func, funcname, args) {
      var fire;
      var namespace = Sage;
      funcname = (funcname === undefined) ? 'init' : funcname;
      fire = func !== '';
      fire = fire && namespace[func];
      fire = fire && typeof namespace[func][funcname] === 'function';

      if (fire) {
        namespace[func][funcname](args);
      }
    },
    loadEvents: function() {
      // Fire common init JS
      UTIL.fire('common');

      // Fire page-specific init JS, and then finalize JS
      $.each(document.body.className.replace(/-/g, '_').split(/\s+/), function(i, classnm) {
        UTIL.fire(classnm);
        UTIL.fire(classnm, 'finalize');
      });

      // Fire common finalize JS
      UTIL.fire('common', 'finalize');
    }
  };

  // Load Events
  $(document).ready(UTIL.loadEvents);

})(jQuery); // Fully reference jQuery after this point.
