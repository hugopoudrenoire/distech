<?php
/**
 * Template Name: Page Specified
 */
?>

<?php while (have_posts()) : the_post(); ?>


    <section id="page-specified" class="wrapper-content">
        <div id="section-concours" class="container-fluid">
            <div class="row">

                <div id="section-1" class="col-sm-12 section left">
                    <div class="outerCenter">
                        <div class="middleCenter">
                            <div class="innerCenter">
                                <h1 class="wow fadeInDown" data-wow-delay="0.2s">It's Time You Specified ECLYPSE!</h1>
                                <p class="wow fadeInUp" data-wow-delay="0.4s">Save time on your next project with our easy-to-use specification builder. Fill out the form below for instant access and to win! Top prizes include $100 Amazon gift cards!</p>
                            </div>
                        </div>
                    </div>
                </div>

                <div id="section-2" class="col-sm-12 section right">
                    <div class="outerCenter">
                        <div class="middleCenter">
                            <div class="innerCenter">
                                <form id="formConcours" action="">
                                    <input type="hidden" name="action" value="add_participant">
                                    <div class="col-xs-12 one-row wow fadeInDown">
                                        <input data-required type="text" name="name" placeholder="Full name">
                                    </div>
                                    <div class="col-xs-12 one-row wow fadeInDown">
                                        <input data-required type="text" name="company" placeholder="Company">
                                    </div>
                                    <div class="col-xs-12 one-row wow fadeInDown">
                                        <input data-required type="text" name="title" placeholder="Title">
                                    </div>
                                    <div class="col-xs-12 one-row wow fadeInDown">
                                        <p class="label-left">Are you a consulting specifying engineer?</p>
                                        <input id="checkEngineer" type="checkbox" bootstrap-checkbox name="question_consulting" checked>
                                        <!-- si lautre en hau est a false -->
                                        <div id="zoneEngineer" class="wrapper-checkbox" style="display: none">
                                            <label for="">
                                                <input type="checkbox" name="systemintegrator" >System Integrator
                                            </label>
                                            <label for="">
                                                <input type="checkbox" name="buildingowner" >Building Owner / Manager
                                            </label>
                                        </div>
                                    </div>
                                    <div class="col-xs-12 one-row wow fadeInDown">
                                        <input data-required type="text" name="country" placeholder="Country">

                                    </div>
                                    <div class="col-xs-12 one-row wow fadeInDown">
                                        <input data-required type="text" name="state" placeholder="State/Province">
                                    </div>
                                    <div class="col-xs-12 one-row wow fadeInDown">
                                        <input data-required type="email" name="email" placeholder="Email">
                                    </div>
                                    <div id="switchNotFloat" class="col-xs-12 one-row more-padding wow fadeInDown">
                                        <p>Do you want a lunch and learn at your office with a local Distech Controls' expert Business Development Manager? <span id="modalLunchClick" class="btn-modal" ><img class="icone-question" src="<?php echo get_template_directory_uri(); ?>/assets/images/ui/icone-question.svg" alt=""></span></p>
                                        <input type="checkbox" bootstrap-checkbox name="question_learn" checked>
                                    </div>
                                    <div class="col-xs-12 one-row more-padding wow fadeInDown">
                                        <p class="label-left">Request a SmartSource login <span id="modalSmartClick" class="btn-modal" ><img class="icone-question" src="<?php echo get_template_directory_uri(); ?>/assets/images/ui/icone-question.svg" alt=""></span><br><span class="bold">(get 5 additional chances to win)</span></p>
                                        <input type="checkbox" bootstrap-checkbox name="smartsource" checked>
                                    </div>
                                    <div class="col-xs-12 one-row more-padding wow fadeInDown">
                                        <p class="label-left">Subscribe to quarterly Bulletin <span id="modalBulletinClick" class="btn-modal" ><img class="icone-question" src="<?php echo get_template_directory_uri(); ?>/assets/images/ui/icone-question.svg" alt=""></span><br><span class="bold">(get 2 additional chances to win)</span></p>
                                        <input type="checkbox" bootstrap-checkbox name="bulletin" checked>
                                    </div>
                                    <div id="codeLinkedin" class="col-xs-12 one-row wow fadeInDown" >
                                        <p>Did you spot the secret password on our LinkedIn page? Claim your prize by entering the password here!</p>
                                        <input type="text" name="linkedinpass" placeholder="Enter your secret LinkedIn password">
                                    </div>
                                    <div class="col-xs-12 one-row button-wrapper wow fadeInDown">
                                        <p id="txtMissingFields" class="txtError">Please fill in all fields</p>                            
                                        <p id="txtBadEmail" class="txtError">Email already in use</p>                            
                                        <button>Get the specs & win</button>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                    
                </div>
            </div>
            <div class="row">
                <div class="wrapper-btn-up col-sm-12 wow bounce infinite">
                    <img id="btn-up" src="<?php echo get_template_directory_uri(); ?>/assets/images/ui/btn-arrow-up.svg" alt="">
                </div>
            </div>
        </div>      
    </section>

    <section id="page-prizes" class="wrapper-content" style="display:none">
	    <div class="container-fluid">
	        <div class="row">
	            <div class="outerCenter">
                    <div class="middleCenter">
                        <div class="innerCenter">
                            <div class="col-sm-12 section section-1">
                                <h1>Congratulations!</h1>
                                <p>You're the winner of <span class="title-price"><?php echo get_option("titre_prixbouteille"); ?></span>
                                     <span id="txtPrix2" >&nbsp;</span> !</p>
                            </div>

                            <div class="col-sm-12 section section-2">
                                <img src="<?php echo pn_get_image_url(get_option("image_prixbouteille")); ?>" alt="">
                                <img id="imgPrix2" src="<?php echo get_template_directory_uri(); ?>/assets/images/prizes/price-2.png" alt="">
                            </div>

                            <div class="col-sm-12 section section-3">
                                <div class="inner">
                                    <p class="sub-title">Claim your prize in person at booth 1242 at AHR Expo by presenting this message to the Distech Controls hostess, stationed by our water bar.</p>
                                    <p>Keep an eye out for your confirmation email, which will include a direct link to the easy-to-use specification builder.</p>
                                    <a href="mailto:?subject=Your friend recommends this easy-to-use specification builder!&body=Looking to save time when specifying your next project? You can access this specification builder, developed by Distech Controls, here : https://secure.distech-controls-corporate.com/SpecBuilder/ %0D%0A%0D%0A
                                     
                                    With just a few clicks, generate your specs, including the latest IP-based solutions!%0D%0A%0D%0A
                                     
                                    For mechanical engineers, this specification builder provides a complete Division 23 or Division 25 spec, in addition to Division 26 and Division 27 guidance documents. %0D%0A%0D%0A
                                     
                                    Unify control and simplify coordination of your work with electrical engineers and IT/communication teams.%0D%0A%0D%0A
                                     
                                    Happy building!%0D%0A%0D%0A
                                     
                                    Learn more by visiting www.distech-controls.com/SpecifyECLYPSE" class="btn-green hidden-sm hidden-md hidden-lg">share the spec builder</a>
                                                        
                                    <a href="<?php echo pn_get_url_from_template("page-map.php"); ?>" class="btn-green">Go to interactive map</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
	        </div>
	    </div>
	</section>



    <!-- MODAL -->
    <div class="modal fade" tabindex="-1" role="dialog" id="modalSmart">
      <div class="modal-dialog">
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
          </div>
          <div class="modal-body">
            <p>SmartSource is designed to provide you with documentation and information on our products and building management solutions.</p>
          </div>
        </div><!-- /.modal-content -->
      </div><!-- /.modal-dialog -->
    </div><!-- /.modal -->

    <div class="modal fade" tabindex="-1" role="dialog" id="modalLunch">
      <div class="modal-dialog">
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
          </div>
          <div class="modal-body">
            <p>Lunch and learns can be a fun, quick way to learn about our latest IoT Building innovations.</p>
          </div>
        </div><!-- /.modal-content -->
      </div><!-- /.modal-dialog -->
    </div><!-- /.modal -->

    <div class="modal fade" tabindex="-1" role="dialog" id="modalBulletin">
      <div class="modal-dialog">
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
          </div>
          <div class="modal-body">
            <p>We would love to share our news, tip and tricks with you. Sign up for our quarterly bulletins, developed specifically with you in mind!</p>
          </div>
        </div><!-- /.modal-content -->
      </div><!-- /.modal-dialog -->
    </div><!-- /.modal -->

    


<?php endwhile; ?>
