<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

$pnPostTypeParticipant = new PnPostType("participant", "Participant");

$pnPostTypeParticipant->set("company", "Company", "text");
$pnPostTypeParticipant->set("title", "Title", "text");
$pnPostTypeParticipant->set("email", "Email", "text");
$pnPostTypeParticipant->set("question_consulting", "Are you a consulting specifying engineer?", "text", array("bg-color"=>"gris1"));
$pnPostTypeParticipant->set("systemintegrator", "System Integrator", "text", array("bg-color"=>"gris1"));
$pnPostTypeParticipant->set("buildingowner", "Building Owner / Manager", "text", array("bg-color"=>"gris1"));
$pnPostTypeParticipant->set("country", "Country", "text", array("bg-color"=>"gris1"));
$pnPostTypeParticipant->set("state", "State", "text", array("bg-color"=>"gris1"));

$pnPostTypeParticipant->set("question_learn", "Do you want a lunch and learn at your office with a local Distech Controls' expert Business Development Manager?", "text", array("bg-color"=>"gris1"));
$pnPostTypeParticipant->set("smartsource", "Request a SmartSource login", "text", array("bg-color"=>"gris1"));
$pnPostTypeParticipant->set("bulletin", "Subscribe to quarterly Bulletin", "text", array("bg-color"=>"gris1"));
$pnPostTypeParticipant->set("linkedinpass", "Secret linkeidin password", "text", array("bg-color"=>"gris1"));




/**
 * 
 * 
 * EXPORT CSV ALL
 * 
 */
add_action('admin_menu', 'register_xml_all_participant');
function register_xml_all_participant() {
	add_submenu_page( 'edit.php?post_type=participant', "CSV Des participants/Gagnants", "CSV des participants/Gagnants" , 'manage_options', 'c-des-participants', "register_xml_all_participant_html" );
}

function register_xml_all_participant_html() {	
    ?>
    <h1>CSV De tous les participants</h1>
    <p><a id="downloadCSV" target="_blank" class="button-primary" href="<?php echo get_template_directory_uri() . "/lib/distech/xml_all_participants.php" ; ?>">Télécharger le CSV</a></p>        
     <br/><br/>   
    <h1>CSV De tous les gagnants</h1>
    <p><a id="downloadCSV" target="_blank" class="button-primary" href="<?php echo get_template_directory_uri() . "/lib/distech/xml_all_gagnant.php" ; ?>">Télécharger le CSV</a></p>        
            
    <?php        
}



/**
 * 
 * 
 * Winner's Stats
 * 
 */
add_action('admin_menu', 'register_winners_stats');
function register_winners_stats() {
	add_submenu_page( 'edit.php?post_type=participant', "Stats des gagnants", "Stats des gagnants" , 'manage_options', 'stats-des-gagnants', "winners_stats_html" );
}

function winners_stats_html() {	
    ?>
    <h1>Gagnant par jour</h1>

    <?php 
    $participants = get_posts(array("post_type"=>"gagnant", "posts_per_page"=>-1, "post_status"=>"any"));
    ?>
    
    <table id="tableSorter">
        <thead>
            <tr>
                <th>ID</th>  
                <th>Nom</th>      
                <th>Email</th>              
                <th>Prix</th>              
                <th>Journée</th>                              
            </tr>
        </thead>
        <tbody>       
    
    <?php
    foreach($participants as $p){
        $idUser = get_post_meta($p->ID, "idGagnant", true);
        printf("<tr>");
        
        printf("<td>%s</td>", $idUser);
        printf("<td>%s</td>", $p->post_title);
        printf("<td>%s</td>", get_post_meta($idUser, "email", true) );
        printf("<td>%s</td>", get_the_title(get_post_meta($p->ID, "idPrix", true)) );
        printf("<td>%s</td>", get_post_meta($p->ID, "day", true) );

        printf("</tr>");
    }   
    ?>
        </tbody>    
    </table>
    <script type='text/javascript'>
        (function($) {
            $("#tableSorter").DataTable(); 
        })(jQuery);
    </script>    
    
    <?php        
}


/**
 * AJOUTE UN PARTICIPANT' TIRAGE ET ENVOI DE EMAIL
 */
add_action( 'wp_ajax_add_participant', 'add_participant' );
add_action( 'wp_ajax_nopriv_add_participant', 'add_participant' );

function add_participant(){
    
    //EMAIL VALIDATION
    $email = isset($_POST["email"]) ? $_POST["email"] : "";
    $parti = get_posts(array("post_type"=>"participant", "post_status"=>"any", "posts_per_page"=>-1));
    foreach($parti as $p){
        $pe = get_post_meta($p->ID, "email", true);
        if(strtolower($email) == strtolower($pe)){
            echo json_encode(array("error"=>1));
            die();
        }        
    }

    
    if(isset($_POST["name"]) ){
        //
        //  INSERT PARRTICIPANT
        //     
        $metaNames = array( "company", "title", "email", "country", "state", "linkedinpass",);
        $metaNamesCheckbox = array( "question_consulting", "systemintegrator",
            "buildingowner", "question_learn","smartsource","bulletin");        
        $metaValues = array();
        
        foreach($metaNames as $val){                                                    //Get values from text filed and checkbox
            $metaValues[$val] = isset($_POST[$val]) ? $_POST[$val] : "";            
        }
        foreach($metaNamesCheckbox as $val){
            $metaValues[$val] = isset($_POST[$val]) ? "yes" : "no";            
        }        
        
        $arrPost = array("post_type"=>"participant", "post_status"=>"private", "post_title"=>$_POST["name"]);   //Save user Information
        $id = wp_insert_post($arrPost);
        
        foreach($metaValues as $key=>$val){
            update_post_meta($id, $key, $metaValues[$key]);
        }        
         
        //
        //  CALC CHANCE / SEND EMAILS 
        //
        $plusDeux = 0;
        $plusCinq = 0;
        if($metaValues["smartsource"] == "yes"){
            $plusCinq = 5;      
            $email_smartsource = get_option("email_smartsource");
            mail_template_info($email_smartsource, "Request for a SmartSource login", "<h1>Request for a SmartSource login</h1>", $metaValues, $_POST["name"]);            
        }
        if($metaValues["bulletin"] == "yes"){
            $plusDeux = 2;            
        }        
        if($metaValues["question_learn"] == "yes"){
            $email_meeting = get_option("email_meeting");
            mail_template_info($email_meeting, "Request for a one-on-one meeting", "<h1>Request for a one-on-one meeting</h1>", $metaValues, $_POST["name"]);            
        }        
        
        $email_completed = get_option("email_formcompleted");
        mail_template_info($email_completed, "New form completed", "<h1>New form completed</h1>", $metaValues, $_POST["name"]);               

        //
        //  TIRAGE
        //
        $instantAmazon = get_option("instantwin_amazon");
        if($instantAmazon == "on" || $instantAmazon == 1){
            tirage_prix_amazon($id, $email);
        }
        
        if(strtoupper($metaValues["linkedinpass"]) == "GO SPECIFY"){
            tirage_prix_password($id, $email);
        }

        tirage_prix($id,$metaValues["email"], $plusDeux, $plusCinq);             
        
    }
    
    die();
}

/**
 * 
 * @param type $idUser
 * @param type $idPrix
 */
function insert_winner($idUser, $idPrix){
    $today = getdate();        
    $indexAujourdhui = $today["mday"] ."/". $today["mon"] ."/". $today["year"];     
    
    $arrPost = array("post_type"=>"gagnant", "post_status"=>"private", "post_title"=>  get_the_title($idUser));   //Save user Information
    $idGagnant = wp_insert_post($arrPost);            
    update_post_meta($idGagnant, "idGagnant", $idUser);    
    update_post_meta($idGagnant, "idPrix", $idPrix);       
    update_post_meta($idGagnant, "day", $indexAujourdhui);           
}


/**
 * Generate HTML when someone win a prize
 */
function get_price_html_for_mail($idPrix){
    $message= "";
    ob_start();
    ?>

    <p>Prize: You're the winner of <span class="title-price"><?php echo get_the_title($idPrix); ?></span></p>
    <img src="<?php echo  pn_get_image_url_from_meta($idPrix, "photo"); ?>" alt="">
        
    <?php
    $message = ob_get_clean(); 
    return $message;
}


/**
 * 
 * @param type $idUser
 */
function tirage_prix_amazon($idUser, $email){
    $today = getdate();        
    $indexAujourdhui = $today["mday"] . $today["mon"] . $today["year"];        
    
    //Trouve le POST du prix spécial
    $prixSpecial = null;
    $prixs = get_posts(array("post_type"=>"prix", "post_status"=>"publish", "posts_per_page"=>-1));
    foreach($prixs as $prix){
        if(strpos(strtoupper($prix->post_title), "AMAZON") !== false){            
            $prixSpecial = $prix;
        }    
    }    
    
    //Si on a trouvé prix "Amazon"    
    if(is_object($prixSpecial)){                        
        //Info pour les quantité des par jour
        $qtyParJour= get_post_meta($prixSpecial->ID, "qtyParJour", true);              //Tableau assosiatif par le date des qty par jou    
        $qty = get_post_meta($prixSpecial->ID, "qty", true);                
        
        //Si la première fois, Crée tableau et index du jour avec QTY de base
        if(!is_array($qtyParJour)) {
            $qtyParJour = array();
            $qtyParJour[$indexAujourdhui] = $qty;     
        }elseif(!isset($qtyParJour[$indexAujourdhui])){
            $qtyParJour[$indexAujourdhui] = $qty;     
        }
        update_post_meta($prixSpecial->ID, "qtyParJour", $qtyParJour);    
        
        //Si encore des PRIX ON GAGNER
        if($qtyParJour[$indexAujourdhui] > 0){            
            $prixGagne=array();            
            $image = pn_get_image_url_from_meta($prixSpecial->ID, "photo");                        
            $prixGagne["produit"] =  array("title" => get_the_title($prixSpecial->ID) , 
                        "image" =>$image); 
            
            $qtyParJour[$indexAujourdhui] = $qtyParJour[$indexAujourdhui] - 1;     
            update_post_meta($prixSpecial->ID, "qtyParJour", $qtyParJour);    
            
            //INSERT GAGNANT
            insert_winner($idUser, $prixSpecial->ID);
 
            //ENVOI MAIL 
            $pTitle = get_the_title($prixSpecial->ID);
            $pImg = pn_get_image_url_from_meta($prixSpecial->ID, "photo");       
            mail_participation($email, $pTitle, $pImg );
            
            echo json_encode($prixGagne);
            
            die();
        }        
    }       
}


/**
 * 
 * @param type $idUser
 */
function tirage_prix_password($idUser, $email){
    $today = getdate();        
    $indexAujourdhui = $today["mday"] . $today["mon"] . $today["year"];        
    
    //Trouve le POST du prix spécial
    $prixSpecial = null;
    $prixs = get_posts(array("post_type"=>"prix", "post_status"=>"publish", "posts_per_page"=>-1));
    foreach($prixs as $prix){
        if(get_post_meta($prix->ID, "special", true) == "oui"){
            $prixSpecial = $prix;
        }    
    }    
    
    if(is_object($prixSpecial)){                    //Si on a trouvé                
        //Info pour les quantité des par jour
        $qtyParJour= get_post_meta($prixSpecial->ID, "qtyParJour", true);              //Tableau assosiatif par le date des qty par jou    
        $qty = get_post_meta($prixSpecial->ID, "qty", true);

        //Si la première fois, Crée tableau et index du jour avec QTY de base
        if(!is_array($qtyParJour)) {
            $qtyParJour = array();
            $qtyParJour[$indexAujourdhui] = $qty;     
        }elseif(!isset($qtyParJour[$indexAujourdhui])){
            $qtyParJour[$indexAujourdhui] = $qty;     
        }
        update_post_meta($prixSpecial->ID, "qtyParJour", $qtyParJour);         

        //Si encore des PRIX ON GAGNER
        if($qtyParJour[$indexAujourdhui] > 0){            
            $prixGagne=array();            
            $image = pn_get_image_url_from_meta($prixSpecial->ID, "photo");                        
            $prixGagne["produit"] =  array("title" => get_the_title($prixSpecial->ID) , 
                        "image" =>$image); 
            
            $qtyParJour[$indexAujourdhui] = $qtyParJour[$indexAujourdhui] - 1;     
            update_post_meta($prixSpecial->ID, "qtyParJour", $qtyParJour);    
            
            //INSERT GAGNANT
            insert_winner($idUser, $prixSpecial->ID);
            
            //ENVOI MAIL 
            $pTitle = get_the_title($prixSpecial->ID);
            $pImg = pn_get_image_url_from_meta($prixSpecial->ID, "photo");              
            mail_participation($email, $pTitle, $pImg );

            echo json_encode($prixGagne);
            
            die();
        }
    }
}

/**
 * 
 * @param type $userID
 * @param type $email
 * @param type $plusDeux
 * @param type $plusCinq
 */
function tirage_prix($userID, $email, $plusDeux=0, $plusCinq=0) {
    $estimationParticipant = get_option("estimation_participant");
    $estimationParticipant = ($estimationParticipant<1) ? 1 : $estimationParticipant;
    $indexDejaGagne = 0+$plusDeux+$plusCinq;
    $prixGagne = array();
    $prixs = get_posts(array("post_type"=>"prix", "posts_per_page"=>-1));
    $sacPigePrix = array_fill(0, $estimationParticipant, 0);
    $indexPrix = 0;
    $today = getdate();        
    $indexAujourdhui = $today["mday"] . $today["mon"] . $today["year"];         
    
    
    //Parcours chaque prix, ajoute prix dans le sav de Pige
    foreach($prixs as $prix){
        //SI CEST UN PRIX HORS TIRAGE
        if(get_post_meta($prix->ID, "special", true) == "oui"){
            continue;
        }           
        //Info prix
        $qty = get_post_meta($prix->ID, "qty", true);   
        $qtyParJour= get_post_meta($prix->ID, "qtyParJour", true);              //Tableau assosiatif par le date des qty par jou
           
        //Si la première fois, Crée tableau et index du jour avec QTY de base
        if(!is_array($qtyParJour)) {
            $qtyParJour = array();
            $qtyParJour[$indexAujourdhui] = $qty;     
        }elseif(!isset($qtyParJour[$indexAujourdhui])){
            $qtyParJour[$indexAujourdhui] = $qty;     
        }
        update_post_meta($prix->ID, "qtyParJour", $qtyParJour);    
        
        //Ajout prix dans sac à pige
        $nextIndexPrix = $indexPrix + $qtyParJour[$indexAujourdhui];
        for($i=$indexPrix; $i<$nextIndexPrix; $i++ ){
            $sacPigePrix[$i] = $prix->ID;
        }
        $indexPrix=$nextIndexPrix;
        $indexDejaGagne += ($qty-$qtyParJour[$indexAujourdhui]); 
        //echo  $indexDejaGagne . " " . ($estimationParticipant - $indexDejaGagne) . " " .$qtyParJour[$indexAujourdhui] .  "\n";                   
    }
    
    //print_r($sacPigePrix);
    
    //Tir dans le sac de pige
    $rangeTirage = ($estimationParticipant - $indexDejaGagne);
    if($rangeTirage<$indexPrix){
        $rangeTirage = $indexPrix-1;
    }
    $chanceUser = rand(0,$rangeTirage);
    
    //SI PLUS DE CHANCE ET QUE PAS GAGNÉ
    if($plusCinq != 0 || $plusDeux != 0){
        if(isset($sacPigePrix[$chanceUser]) && $sacPigePrix[$chanceUser] == 0){
            $indexChance =0;
            while($indexChance<($plusCinq+$plusDeux)){
                $chanceUser = rand(0,$rangeTirage);
                $indexChance++;
                //Si winner, break
                if(isset($sacPigePrix[$chanceUser]) && $sacPigePrix[$chanceUser] != 0){
                    break;
                }
            }
        }
    }
    
    
    //
    //  SI GAGNER
    //
    if(isset($sacPigePrix[$chanceUser]) && $sacPigePrix[$chanceUser] != 0){
        $idProduitGagne = $sacPigePrix[$chanceUser];
        $image = pn_get_image_url_from_meta($idProduitGagne, "photo");
        $prixGagne["produit"] =  array("title" => get_the_title($idProduitGagne) , 
                                        "tirage"=>$chanceUser,
                                        "image" =>$image,
            "r"=>$rangeTirage);   
        
        //INSERT GAGNANT
        insert_winner($userID, $idProduitGagne);        
        
        //Reduce QTY;
        $qtyParJour= get_post_meta($idProduitGagne, "qtyParJour", true);              //Tableau assosiatif par le date des qty par jou
        $qtyParJour[$indexAujourdhui] = $qtyParJour[$indexAujourdhui] - 1;     
        update_post_meta($idProduitGagne, "qtyParJour", $qtyParJour);            
            
        //ENVOI MAIL
        $pTitle = get_the_title($idProduitGagne);
        $pImg = pn_get_image_url_from_meta($idProduitGagne, "photo");           
        mail_participation($email, $pTitle, $pImg );           
    }else{
        $prixGagne["info"]["tirage"] =   $chanceUser; 
        
        //ENVOI MAIL
        mail_participation($email );        
    }
    
    
 
    
    echo json_encode($prixGagne);
    
}



function mail_participation($email, $prixTitle="", $prixImg=""){
    //RÉDACTION DU EMAIL
    $headers = array();
    $headers[] = 'From: Distech Controls <info@gospecify.com>';
    $headers[] = 'Content-Type: multipart/alternative';            
    $headers[] = 'Content-Type: text/html; charset=UTF-8';        

    $sujet = "It’s Time You Specified ECLYPSE!";
    
    ob_start();
    ?>

    <!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd"> 
    <html xmlns="http://www.w3.org/1999/xhtml">
    <head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
    <style type="text/css">
        h2 {
            font-family: 'Arial', sans-serif;
            font-size:22px;
            color:#033681;
            line-height:28px;
            display:inline-block;
            margin: 0px;
            
        }

        p {
            font-family: 'Arial', sans-serif;
            color:#818181;
            font-size:15px;
            margin:20px 0px;
        }

        .title-price{
            color:#033681;
            text-transform: uppercase;
        }

    </style>
    </head>
    <body style="width:100%; margin:0; padding:0; -webkit-text-size-adjust:100%; -ms-text-size-adjust:100%;">
    <table cellpadding="0" cellspacing="0" border="0" style="margin:0; padding:0; width:100%; background: #fafafa;">
      <tr>
        <td valign="top">
          <!-- edge wrapper -->
          <table cellpadding="0" cellspacing="0" border="0" align="center" width="600" style="background: #ffffff;">
              <tr>
                  <td colspan="2">
                    <img style="width:600px; display:block;" src="<?php echo get_template_directory_uri(); ?>/assets/images/ui/face.jpg" alt="">                      
                  </td>
              </tr>
              <tr style="background-color:#3391EC">
                  <td colspan="2"><p style="color:white; font-size:20px; margin:28px 0px; text-align:center; font-weight:bold; letter-spacing: 3px;">It’s Time You Specified ECLYPSE!</p></td>
              </tr>
              <tr>
                <td valign="top" style="padding:65px" colspan="2">
                  <h2 style="margin-top:0px;">Thank you for participating in the<br/> "It's Time You Specified ECLYPSE"<br/> contest.</h2>
                  <p>You're the winner of <span class="title-price" style='color:#033681; text-transform: uppercase; font-weight:bold;'><?php echo get_option("titre_prixbouteille"); ?></span>
                    <?php
                    if($prixTitle != ""){
                        printf("and <span class='title-price' style='color:#033681; text-transform: uppercase; font-weight:bold;'>%s</span>", $prixTitle);
                    }                    
                    ?>
                   !</p>
                  
                  <p>Claim your prize in person at booth 1242 at AHR Expo by presenting this message to the Distech Controls hostess, stationed by our water bar.</p>
  
                </td>                  
            </tr>
            <tr>
                <td style="text-align: center;">
                    <img  style='width:250px' src="<?php echo pn_get_image_url(get_option("image_prixbouteille")); ?>" alt="">
                </td>
                <td style="text-align: center;">
                    <?php
                       if($prixImg != ""){
                           printf("<img style='width:250px' src='%s' />", $prixImg);
                       }                    
                    ?>
                </td>
            </tr>
            <tr>
                <td colspan="2">
                    <hr style="height:2px; background-color:#EFEFEF; border:0px"/>                      
                </td>
            </tr>
            <tr>
                <td valign="top" style="padding:65px" colspan="2">
                  <h2 style="margin-top:0px">Looking to save time when<br/> specifying your next project?</h2>
                     
                    <p>Access this new specification builder, developed by Distech Controls.</p>

                    <p>With just a few clicks, generate your specs, including the latest IP-based solutions!</p>

                    <p>For mechanical engineers, this specification builder provides<br/>
                        a complete Division 23 or Division 25 spec, in addition to<br>
                        Division 26 and Division 27 guidance documents. </p>

                    <p>Unify control and simplify coordination of your work with<br/>
                        electrical engineers and IT/communication teams.</p>

                    <p>Happy building!</p>
                  
                    <p style="margin-bottom:0px; text-align:center">
                        <!-- <a style="display:inline-block;  padding:20px 30px; background-color:#62BD51; color:white; border-radius: 25px; text-decoration:none;" 
                          href="http://www.distech-controls.com/SpecifyECLYPSE">GET THE SPECS</a> -->
                          <a style="display:inline-block; background-color:#62BD51; color:white; border-radius: 25px; text-decoration:none;" 
                          href="https://secure.distech-controls-corporate.com/SpecBuilder/">
                            <img src="<?php echo get_template_directory_uri(); ?>/assets/images/ui/btn-specs.png" alt="" />
                          </a>
                    </p>                                   
                </td>                  
            </tr>
            <tr>
                <td colspan="2">
                    <hr style="height:4px; background-color:#EFEFEF; border:0px"/>                      
                </td>
            </tr>
            <tr>
                <td valign="top" style="padding:30px" style="width: 50%;">
                    <a href="http://www.distech-controls.com"><img src="<?php echo get_template_directory_uri(); ?>/assets/images/ui/logo_col.jpg" alt=""></a>
               </td>
               <td valign="middle" style="padding:30px" style="width: 50%;">
                   <a style="display:inline-block;" href="https://www.facebook.com/distechcontrols"><img src="<?php echo get_template_directory_uri(); ?>/assets/images/ui/logo-fb.png" alt=""></a>
                   <img style="display:inline-block;" src="<?php echo get_template_directory_uri(); ?>/assets/images/ui/push.png" alt="">
                   <a style="display:inline-block;" href="https://www.linkedin.com/company/distech-controls"><img src="<?php echo get_template_directory_uri(); ?>/assets/images/ui/logo-in.png" alt=""></a>
                   <img style="display:inline-block;" src="<?php echo get_template_directory_uri(); ?>/assets/images/ui/push.png" alt="">
                   <a style="display:inline-block;" href="https://twitter.com/DistControls_en"><img src="<?php echo get_template_directory_uri(); ?>/assets/images/ui/logo-tw.png" alt=""></a>
                   <img style="display:inline-block;" src="<?php echo get_template_directory_uri(); ?>/assets/images/ui/push.png" alt="">
                   <a style="display:inline-block;" href="https://www.youtube.com/user/distechcontrols"><img src="<?php echo get_template_directory_uri(); ?>/assets/images/ui/logo-yt.png" alt=""></a>
               </td>              
              </tr>
          </table>
        </td>
      </tr>
    </table>  
    </body>
    </html>  
    
    <?php
    $message = ob_get_clean();        

    $rep = wp_mail($email, stripslashes($sujet), stripslashes($message), $headers);    
}





function mail_template_info($email, $sujet, $text , $info, $name){
    //RÉDACTION DU EMAIL
    $headers = array();
    $headers[] = 'From: Distech Controls<info@gospecify.com>';
    $headers[] = 'Content-Type: multipart/alternative';            
    $headers[] = 'Content-Type: text/html; charset=UTF-8';        

    
    ob_start();
    ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd"> 
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="viewport" content="width=device-width, initial-scale=1.0"/>
<style type="text/css">
	
    h1,h2{
        text-align:center;
        color:#506B92;
        
    }
</style>
</head>
<body style="width:100%; margin:0; padding:0; -webkit-text-size-adjust:100%; -ms-text-size-adjust:100%;">

<!-- body wrapper -->
<table cellpadding="0" cellspacing="0" border="0" style="margin:0; padding:0; width:100%; background: #fafafa;">
  <tr>
    <td valign="top">
      <!-- edge wrapper -->
      <table cellpadding="0" cellspacing="0" border="0" align="center" width="600" style="background: #ffffff;">
        <tr style="background-color:#24426F; ">
            <td>
                <p style="text-align:center; padding:10px;"><img src="<?php echo get_template_directory_uri(); ?>/assets/images/ui/logo.png" alt="">    </p>                            
            </td>
        </tr>
        <tr>
          <td valign="top">

            
            <?php echo $text; ?>
            <?php
                printf("<p>Name : %s</p>",$name );
            
            
                $metaNames = array( "company" =>"Company",
                    "title" => "Title",
                    "email" => "Email", 
                    "country" => "Country",
                    "state" => "State",
                    "linkedinpass" => "Linkedin Pass",);
                $metaNamesCheckbox = array( "question_consulting" => "Are you a consulting specifying engineer?",
                    "systemintegrator" =>"System Integrator",
                    "buildingowner" => "Building Owner / Manager", 
                    "question_learn" =>"Do you want a lunch and learn at your office with a local Distech Controls' expert Business Development Manager?",
                    "smartsource" => "Request a SmartSource Login",
                    "bulletin" => "Subscribe to quarterly Bulletin");        

                //Get values from text filed and checkbox
                foreach($metaNames as $key=>$label){
                    $val = isset($info[$key]) ? $info[$key] : "";     
                    printf("<p>%s : %s</p>",$label, $val);
                }
                foreach($metaNamesCheckbox as $key=>$label){
                    $val = isset($info[$key]) ? $info[$key] : "";     
                    printf("<p>%s : %s</p>",$label, $val);
                }

                            
            ?>    
          </td>
        </tr>
      </table>
      <!-- / edge wrapper -->
    </td>
  </tr>
</table>  
<!-- / page wrapper -->
</body>
</html>        
        
        
    <?php
    $message = ob_get_clean();        


    $rep = wp_mail($email, stripslashes($sujet), stripslashes($message), $headers);    
}