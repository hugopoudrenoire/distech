<?php
define('WP_USE_THEMES', false);
require('../../../../../wp-blog-header.php');
status_header(200);

header('Content-Type: application/csv');
header('Content-Disposition: attachment; filename=distech_courriels.csv');
header("Content-Transfer-Encoding: text/csv\n"); 
header('Pragma: no-cache');

/* Short and sweet */
ini_set('display_errors',1);
ini_set('display_startup_errors',1);
error_reporting(-1);


    //PRINT HEADER
    echo utf8_decode("no; Courriel; Date; \n")   ;
        
    //CHECK ALL CONCOURS
    $lesParticipants = get_posts(array("post_type"=>"courriel", "post_status"=>"any", "posts_per_page"=>-1));
    foreach($lesParticipants as $participant){                  

        printf("%s ;",  utf8_decode(($participant->ID)) );      
        printf("%s ;",  utf8_decode($participant->post_title ));                                                              
        printf("%s ; \n",  utf8_decode($participant->post_date ) );                                                          
       
    }

?>
