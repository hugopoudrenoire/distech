<?php


$pnPostTypeCourriel= new PnPostType("courriel", "Courriels");


/**
 * 
 * 
 * EXPORT CSV ALL
 * 
 */
add_action('admin_menu', 'register_xml_all_courriels');
function register_xml_all_courriels() {
	add_submenu_page( 'edit.php?post_type=courriel', "CSV Des Courriels", "CSV Des Courriels" , 'manage_options', 'c-des-courriels', "register_xml_all_courriels_html" );
}

function register_xml_all_courriels_html() {	
    ?>
    <h1>CSV De tous les courriels</h1>
    <p><a id="downloadCSV" target="_blank" class="button-primary" href="<?php echo get_template_directory_uri() . "/lib/distech/xml_all_courriels.php" ; ?>">Télécharger le CSV</a></p>        

    <?php        
}



/**
 * AJOUTE UN PARTICIPANT' TIRAGE ET ENVOI DE EMAIL
 */
add_action( 'wp_ajax_add_courriel', 'add_courriel' );
add_action( 'wp_ajax_nopriv_add_courriel', 'add_courriel' );

function add_courriel(){
       
    if(isset($_POST["courriel"]) ){        
        $arrPost = array("post_type"=>"courriel", "post_status"=>"private", "post_title"=>$_POST["courriel"]);   //Save user Information
        $id = wp_insert_post($arrPost);      
        echo $id;
    }
    
    die();
}
