<?php
define('WP_USE_THEMES', false);
require('../../../../../wp-blog-header.php');
status_header(200);

header('Content-Type: application/csv');
header('Content-Disposition: attachment; filename=distech_participants.csv');
header("Content-Transfer-Encoding: text/csv\n"); 
header('Pragma: no-cache');

/* Short and sweet */
ini_set('display_errors',1);
ini_set('display_startup_errors',1);
error_reporting(-1);


    //PRINT HEADER
    echo utf8_decode("Nom; Company; Title; Email; consulting specifying engineer?; System Integrator; Building Owner / Manager; Country; State; Do you want a lunch ?; Request a SmartSource login; Subscribe to quarterly Bulletin;linkedinpass;  \n")   ;
        
    //CHECK ALL CONCOURS
    $lesParticipants = get_posts(array("post_type"=>"participant", "post_status"=>"any", "posts_per_page"=>-1));
    foreach($lesParticipants as $participant){                  

        printf("%s ;",  utf8_decode(($participant->post_title)) );      
        printf("%s ;",  utf8_decode(get_post_meta($participant->ID, "company", true)) );      
        printf("%s ;",  utf8_decode(get_post_meta($participant->ID, "title", true)) );      
        printf("%s ;",  utf8_decode(get_post_meta($participant->ID, "email", true)) );      
        printf("%s ;",  utf8_decode(get_post_meta($participant->ID, "question_consulting", true)) );      
        printf("%s ;",  utf8_decode(get_post_meta($participant->ID, "systemintegrator", true)) );     
        printf("%s ;", utf8_decode( get_post_meta($participant->ID, "buildingowner", true)) );      

        printf("%s ;", utf8_decode( get_post_meta($participant->ID, "country", true)) );      
        printf("%s ;", utf8_decode( get_post_meta($participant->ID, "state", true)) );      
        
        printf("%s ;", utf8_decode( get_post_meta($participant->ID, "question_learn", true)) );      
        printf("%s ;",  utf8_decode(get_post_meta($participant->ID, "smartsource", true)) );     
        printf("%s ;",  utf8_decode(get_post_meta($participant->ID, "bulletin", true)) );                                                          
        printf("%s ; \n",  utf8_decode(get_post_meta($participant->ID, "linkedinpass", true)) );                                                          
        
        
        
    }

?>
