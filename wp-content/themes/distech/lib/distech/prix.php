<?php



$pnPostTypePrix = new PnPostType("prix", "Prix");

$pnPostTypePrix->set("photo", "Photo (250px / 250px)", "image");

$pnPostTypePrix->set("special", "Prix spécial hors tirage (clé linkedin)", "select", array("values"=>array("non", "oui")));
$pnPostTypePrix->set("qty", "Quantité par jour", "text");