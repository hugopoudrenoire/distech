<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

$pnPostTypeProduit = new PnPostType("produit", "Produits");
$pnPostTypeProduit->addTaxonomy("categorieproduit", "Catégorie");
$pnPostTypeProduit->set("image", "Image du produit (2381px / 830px)", "image");
$pnPostTypeProduit->set("video", "ID vimeo de la vidéo produit", "text");
$pnPostTypeProduit->set("titrenew", "Titre What's new (Sec. 3)", "text", array("bg-color"=>"gris1"));
$pnPostTypeProduit->set("textenew", "Texte What's new (Sec. 3)", "textarea", array("bg-color"=>"gris1"));
$pnPostTypeProduit->set("titrebenefit", "Titre Benefits (Sec. 4)", "text", array("bg-color"=>"gris2"));
$pnPostTypeProduit->set("textebenefit", "Texte Benefits (Sec. 4)", "textarea", array("bg-color"=>"gris2"));
$pnPostTypeProduit->set("awards", "Awards - images (250px / 250px) (Sec. 5)", "gallery");
$pnPostTypeProduit->set("titrefeatures", "Titre Features (Sec. 6)", "text", array("bg-color"=>"gris1"));
$pnPostTypeProduit->set("textefeatures", "Texte Features (Sec. 6)", "textarea", array("bg-color"=>"gris1"));
$pnPostTypeProduit->set("softwares", "Ajout Complementary softwares", "multiple_posttype_link", array("posttype"=>"produit"));

$pnPostTypeProduit->set("thumbnail", "Thumbnail du produit (250px / 250px)", "image");





function display_tax_categorieproduit($tag){
    $t_id = $tag->term_id; // Get the ID of the term you're editing  

    $val = get_option("tax_produit_color_$t_id");
    pn_display_meta_text("tax_produit_color", $val, "Classe de la couleur ");    
    
    $val = get_option("tax_produit_image_$t_id");
    pn_display_meta_wp_one_image("tax_produit_image", $val, "Image ");
    
    $val = get_option("tax_produit_image2_$t_id");
    pn_display_meta_wp_one_image("tax_produit_image2", $val, "Image #2 (Pour catégorie #2)");
    
    $val = get_option("tax_produit_image3_$t_id");
    pn_display_meta_wp_one_image("tax_produit_image3", $val, "Image #3 (Pour catégorie #2)");    
}


function save_tax_categorieproduit($id){
    if(isset($_POST["tax_produit_image"])){
        update_option("tax_produit_image_$id", $_POST["tax_produit_image"]);
    }
    if(isset($_POST["tax_produit_image2"])){
        update_option("tax_produit_image2_$id", $_POST["tax_produit_image2"]);
    }
    if(isset($_POST["tax_produit_image3"])){
        update_option("tax_produit_image3_$id", $_POST["tax_produit_image3"]);
    }    
    if(isset($_POST["tax_produit_color"])){
        update_option("tax_produit_color_$id", $_POST["tax_produit_color"]);
    }      
}

// Add the fields to the "presenters" taxonomy, using our callback function  
add_action( 'categorieproduit_edit_form_fields', 'display_tax_categorieproduit', 10, 2 );  
// Save the changes made on the "presenters" taxonomy, using our callback function  
add_action( 'edited_categorieproduit', 'save_tax_categorieproduit', 10, 2 );  


//Get Read more content
add_action( 'wp_ajax_produit_read_more', 'produit_read_more' );
add_action( 'wp_ajax_nopriv_produit_read_more', 'produit_read_more' );
function produit_read_more() {
    if(isset($_POST['id']) && is_numeric($_POST['id'])){
        $id = $_POST['id'];
        $produit = get_post($id);
        
        $postMetasName = array("image", "video", "titrenew", "textenew", "titrebenefit", "textebenefit",
            "awards", "titrefeatures", "textefeatures", "softwares", "thumbnail");
        
        foreach($postMetasName as $name){
            $$name = get_post_meta($id, $name, true);
        }
        
        //Get awards image
        $awardsImgs = array();
        if($awards != ""){
            $arrAwards = explode(",", $awards);
            if(is_array($arrAwards)){
                foreach($arrAwards as $imgID){
                    $awardsImgs[] = pn_get_image_url($imgID);
                }
            }
        }
        
        ob_start();
?>   
        <div id="product-infos" class="col-sm-12">
            <p><?php echo $produit->post_content; ?></p>
        </div>


        <?php if($video != ""){ ?>
        <div id="product-video">
            <iframe id="iframeVimeo" data-title="<?php echo $produit->post_title; ?>" src="http://player.vimeo.com/video/<?php echo $video; ?>?title=0&byline=0&portrait=0" width="100%" height="280" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>
        </div>
        <?php } ?>


        <?php if($titrenew != ""){ ?>
        <div id="buzz" class="col-sm-12">
            <p class="title"><?php echo $titrenew; ?></p>
            <blockquote>
               <p><img src="<?php echo get_template_directory_uri(); ?>/assets/images/new/quotes-first.png"><?php echo $textenew; ?></p>
            </blockquote>
        </div>
        <?php } ?>

        <?php if($titrebenefit != ""){ ?>
        <div id="benefit" class="col-sm-12">
            <p class="title"><?php echo $titrebenefit; ?></p>
            <ul>
                <?php echo $textebenefit; ?>
            </ul>
            <!-- <div class="wrapper-btn">
                <a href="<?php echo pn_get_url_from_template("page-specified.php"); ?>" class="btn-green">Get the specs & win</a>
            </div> -->
        </div>
        <?php } ?>

        <?php if(count($awardsImgs) > 0){ ?>
        <div id="awards" class="col-sm-12">
            <p class="title">Awards</p>
            <ul>
            <?php  
            foreach($awardsImgs as $img){
                printf('<li><img src="%s" alt=""></li>', $img);
            }
            ?>
            </ul>
        </div>
        <?php } ?>


        <?php if($titrefeatures != ""){ ?>
        <div id="features" class="col-sm-12">
            <p class="title"><?php echo $titrefeatures; ?></p>
            <ul>
                <?php echo $textefeatures; ?>
            </ul>
        </div>
        <?php } ?>


        <?php if(is_array($softwares)){ ?>
        <div id="related" class="col-sm-12">
            <p class="title">Software and Tools</p>
            <ul>
                <?php foreach($softwares as $software){ ?>
                <li>
                    <a href="<?php echo get_permalink($software); ?>">
                        <img src="<?php echo pn_get_image_url_from_meta($software, "thumbnail"); ?>" alt="">
                        <p class="name"><?php echo get_the_title($software); ?></p>
                    </a>
                </li>
                <?php } ?>
                
            </ul>
        </div>
        <?php } ?>

     <?php   
        $out = ob_get_clean();        
        echo $out;
    }

       
	die();
}
