<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
$pnDistech = new PnOption("distech_option", "Option général");

$pnDistech->set("titre_prixbouteille", "Titre du prix que tout le monde gagne", "text");
$pnDistech->set("image_prixbouteille", "Image du prix que tout le monde gagne", "image");


$pnDistech->set("estimation_participant", "Estimation du nombre de participant", "text");
$pnDistech->set("instantwin_amazon", "Faire gagné la carte amazon", "checkbox");

$pnDistech->set("email_smartsource", "Email de confirmation SmartSource", "text");
$pnDistech->set("email_meeting", "Email de confirmation one-on-one Meeting", "text");
$pnDistech->set("email_formcompleted", "Email de confirmation d'un formulaire complété", "text");