<?php
define('WP_USE_THEMES', false);
require('../../../../../wp-blog-header.php');
status_header(200);

header('Content-Type: application/csv');
header('Content-Disposition: attachment; filename=distech_gagnant.csv');
header("Content-Transfer-Encoding: text/csv\n"); 
header('Pragma: no-cache');

/* Short and sweet */
ini_set('display_errors',1);
ini_set('display_startup_errors',1);
error_reporting(-1);


    //PRINT HEADER
    echo utf8_decode("id; Nom; Email; Prix; Journée; \n")   ;
        
    //CHECK ALL CONCOURS
    $lesGagnants = get_posts(array("post_type"=>"gagnant", "post_status"=>"any", "posts_per_page"=>-1));
    foreach($lesGagnants as $gagnant){                  
        $idUser = get_post_meta($gagnant->ID, "idGagnant", true);

        printf("%s ;",  utf8_decode(($gagnant->ID)) );      
        printf("%s ;",  utf8_decode(($gagnant->post_title)) );      
        printf("%s ;",  utf8_decode(get_post_meta($idUser, "email", true)) );      
        printf("%s ;",  utf8_decode(get_the_title(get_post_meta($gagnant->ID, "idPrix", true))) );      
        printf("%s ; \n",  utf8_decode(get_post_meta($gagnant->ID, "day", true)) );              
        
    }

?>
