<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

$pnPostTypeSondage = new PnPostType("sondage", "Sondage");

$pnPostTypeSondage->set("note", "Note", "text");






add_action( 'wp_ajax_add_note', 'add_note' );
add_action( 'wp_ajax_nopriv_add_note', 'add_note' );

function add_note(){
    if(isset($_POST["note"]) ){
        $note = $_POST["note"];

        $postarr = array("post_type" => "sondage", "post_status"=>"private", "post_title"=>"Vote");
        $id = wp_insert_post($postarr);
        update_post_meta($id, "note", $note);
    }
    
    die();
}



/**
 * 
 * 
 * EXPORT CSV ALL
 * 
 */
add_action('admin_menu', 'sondage_result');
function sondage_result() {
	add_submenu_page( 'edit.php?post_type=sondage', "Résultats", "Résultats" , 'manage_options', 'Resultats', "sondage_result_html" );
}

function sondage_result_html() {	
    ?>
    <h2>Résultats des sondagees</h2>

    <?php 
    
    $count=0;
    $total=0;
    $totalByNum = array(0,0,0,0,0,0);
    $posts = get_posts(array("post_type"=>"sondage", "post_status"=>"any", "posts_per_page"=>-1));
    foreach($posts as $p){
        $note = get_post_meta($p->ID, "note" ,  true);
        if(is_numeric($note)){
            $count++;
            $total += $note; 
            $totalByNum[$note]++;            
        }        
    }
    $resultat = $total / $count;
    
    printf("<h3>Nombre d'entrée : %s</h3>", $count);
    printf("<h3>Moyenne des entrées : %s</h3>", $resultat);
    for($i=1; $i<6;$i++){
        printf("<h4>Nombre de vote a %s étoile(s) : %s</h4>", $i, $totalByNum[$i]);
        
    }

    ?>
    
    <?php        
}
