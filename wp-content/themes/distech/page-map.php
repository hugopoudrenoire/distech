<?php
/**
 * Template Name: Page Map
 */
?>

<?php while (have_posts()) : the_post(); ?>


    <section id="page-map" class="fit-screen">
        <div class="container-fluid">
            <div class="row">

                <div id="fix-bg"></div>

                <div class="col-sm-12">
                    <div class="titles">
                        <h1 class="wow">Interactive Map</h1>
                        <p class="desc">Click on the dots to explore our booth and discover our latest products and innovations!</p>
                    </div>

                    <div class="inner">
                        <!--  -->
                        <div id="top-top" class="booth-section wow fadeInLeft" data-wow-delay="0.2s">
                            <span class="info"><span class="more">+</span></span>
                            <span class="txt txt-info">Win with what's new!</span>
                            <img src="<?php echo get_template_directory_uri(); ?>/assets/images/map/top-top.svg" alt="">
                        </div>
                        <!--  -->
                        <!--  -->
                        <div id="top-left" class="booth-section wow fadeInLeft" data-wow-delay="0.2s">
                            <span class="point green left" data-link="<?php echo get_term_link(get_term(5, "categorieproduit")); ?>" data-image="<?php echo pn_get_image_url(get_option("tax_produit_image_5")); ?>"
                                  data-tracking="Map|Click|2 – Demo Stations" data-category-color="green" data-category-title="2 – Demo Stations">2</span>
                            
                            <span class="point green top" data-link="<?php echo get_term_link(get_term(5, "categorieproduit")); ?>" data-image="<?php echo pn_get_image_url(get_option("tax_produit_image2_5")); ?>"
                                  data-tracking="Map|Click|2 – Demo Stations" data-category-color="green" data-category-title="2 – Demo Stations">2</span>
                            <img src="<?php echo get_template_directory_uri(); ?>/assets/images/map/top-left.svg" alt="">
                        </div>
                        <div id="top-right" class="booth-section wow fadeInRight">
                            <span class="point corail top" data-link="<?php echo get_term_link(get_term(6, "categorieproduit")); ?>" data-image="<?php echo pn_get_image_url(get_option("tax_produit_image_6")); ?>"
                                 data-tracking="Map|Click|3 – Smart Room Control" data-category-color="corail" data-category-title="3 – Smart Room Control">3</span>
                            
                            <span class="point orange middle" data-link="<?php echo get_term_link(get_term(7, "categorieproduit")); ?>" data-image="<?php echo pn_get_image_url(get_option("tax_produit_image_7")); ?>"
                                 data-tracking="Map|Click|4 – IP-based Terminal Equipment Control" data-category-color="orange" data-category-title="4 – IP-based Terminal Equipment Control">4</span>
                            
                            <span class="point yellow bottom" data-link="<?php echo get_term_link(get_term(8, "categorieproduit")); ?>" data-image="<?php echo pn_get_image_url(get_option("tax_produit_image_8")); ?>"
                                data-tracking="Map|Click|5 – IP-Based Mechanical Equipment Control"  data-category-color="yellow" data-category-title="5 – IP-Based Mechanical Equipment Control">5</span>
                            
                            <span class="point purple right" data-link="<?php echo get_term_link(get_term(9, "categorieproduit")); ?>" data-image="<?php echo pn_get_image_url(get_option("tax_produit_image_9")); ?>"
                                data-tracking="Map|Click|6 – ECLYPSE – 320 points!"   data-category-color="purple" data-category-title="6 – ECLYPSE – 320 points!">6</span>
                            
                            <img src="<?php echo get_template_directory_uri(); ?>/assets/images/map/top-right.svg" alt="">
                        </div>
                        <div id="bottom-left" class="booth-section wow fadeInLeft">
                            <span class="point lightblue top" data-link="<?php echo get_term_link(get_term(4, "categorieproduit")); ?>" data-image="<?php echo pn_get_image_url(get_option("tax_produit_image_4")); ?>"
                                data-tracking="Map|Click|1 – IP-Based Unified Solution"    data-category-color="lightblue" data-category-title="1 – IP-Based Unified Solution">1</span>
                            <span class="question bottom"><img class="gift" src="<?php echo get_template_directory_uri(); ?>/assets/images/ui/icone-gift.svg" alt=""></span>

                            <p class="txt txt-question">Redeem your prizes here!</p>
                            <img class="bg" src="<?php echo get_template_directory_uri(); ?>/assets/images/map/bottom-left.svg" alt="">
                        </div>
                        <div id="bottom-right" class="booth-section wow fadeInRight" data-wow-delay="0.2s">
                            <span class="point green top" data-category-color="green" data-link="<?php echo get_term_link(get_term(5, "categorieproduit")); ?>"  data-image="<?php echo pn_get_image_url(get_option("tax_produit_image3_5")); ?>"
                                data-tracking="Map|Click|2 – Demo Stations"  data-category-title="2 – Demo Stations">2</span>

                            <img src="<?php echo get_template_directory_uri(); ?>/assets/images/map/bottom-right.svg" alt="">
                        </div>
                        <!--  -->
                    </div>
                </div>

                <div id="pop-up">
                    <a href="" class="category-link"></a>
                    <div id="section" class="">
                        <span class="category">
                            <p id="title"></p>
                        </span>
                        <span class="cancel"><img src="<?php echo get_template_directory_uri(); ?>/assets/images/ui/x-white.svg" alt=""></span>
                    </div>
                    <div id="picture" style=""></div>
                </div>
            </div>
        </div>
    </section>

<?php endwhile; ?>
