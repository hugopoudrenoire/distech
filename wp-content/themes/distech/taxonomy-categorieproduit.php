
<?php  
global $query_string;
query_posts($query_string. '&suppress_filters=1'); 
?>

<?php 
$color = "corail";
if( is_tax() ) {
    global $wp_query;
    $term = $wp_query->get_queried_object();
    $color = get_option("tax_produit_color_$term->term_id");
} 
$taxName ="";
$taxID= "";
if(is_single()){
    $taxs = wp_get_post_terms($post->ID, "categorieproduit");
    foreach ($taxs as $tax){
        $taxName = $tax->name;
        $taxID=  $tax->term_id;

        $color = get_option("tax_produit_color_$tax->term_id");        
        break;
    }    
}
?>


    <section id="page-products" class="wrapper-content">
        <div class="container-fluid">
            <div class="row">
				
				<div id="nav">
					<div class="back wow fadeInLeft">
						<div class="outerCenter">
							<div class="middleCenter">
								<div class="innerCenter">
									<a href="<?php echo pn_get_url_from_template("page-map.php"); ?>">Back to map</a>
								</div>
							</div>
						</div>
					</div>
					<!-- changer la couleur de la class selon la category -->
					<div class="category <?php echo $color; ?> wow fadeInRight">
						<div class="outerCenter">
							<div class="middleCenter">
								<div class="innerCenter">
									<a href="">
                                    <?php 
                                    //CHECK IF SINGLE-PRODUIT
                                    if(is_single()){
                                         echo $taxName;
                                    }
                                    else if( is_tax() ) {
                                        echo $term->name;
                                    } 
                                    ?>
                                    </a>
								</div>
							</div>
						</div>
					</div>
				</div>
                
                <div id="carousel-product" class="carousel slide" data-ride="carousel" data-interval="false">
                    <div class="carousel-inner" role="listbox">
                        <?php 
                        $count=0;
                        while (have_posts()) : the_post(); 
                        
                        $image = pn_get_image_url_from_meta(get_the_ID(), "image");
                        
                        ?>
                        <div data-id="<?php echo get_the_ID(); ?>" data-title="<?php echo get_the_title(); ?>" class="item <?php if($count==0) echo "active"; ?>">
                            <div id="product-picture">
                                <!-- style="background-image: url('<?php echo get_template_directory_uri(); ?>/assets/images/new/produit-big.jpg');" -->
                                <img src="<?php echo $image; ?>" alt="">
                            </div>

                            <div class="loader-bar">
			                	<div class="bar"></div>
			                </div> 

                            <div id="product-title" class="col-sm-12">
                                <p class="title"><?php echo get_the_title(); ?></p>
                            </div>                        
                        </div>                    
                        <?php 
                        $count++;
                        endwhile;
                        
                        //SI SINGLE, LOAD LES AUTRES
                        if(is_single()){
                            
                            $args = array(
                                "post_type"=>"produit",
                                "post__not_in"=>array(get_the_ID()),
                                'tax_query' => array(
                                    array(
                                        'taxonomy' => 'categorieproduit',
                                        'field' => 'term_id',
                                        'terms' => $taxID
                                    )
                                ),
                                "posts_per_page"=>-1
                            );
                            $postslist = get_posts( $args );    
                            foreach($postslist as $po){
                                $image = pn_get_image_url_from_meta($po->ID, "image");
                                
                            ?>
                                
                        <div data-id="<?php echo $po->ID; ?>" data-title="<?php echo $po->post_title; ?>" class="item">
                            <div id="product-picture">
                                <!-- style="background-image: url('<?php echo get_template_directory_uri(); ?>/assets/images/new/produit-big.jpg');" -->
                                <img src="<?php echo $image; ?>" alt="">
                            </div>

                            <div class="loader-bar">
			                	<div class="bar"></div>
			                </div> 

                            <div id="product-title" class="col-sm-12">
                                <p class="title"><?php echo $po->post_title; ?></p>
                            </div>                        
                        </div>                                  
                            <?php                                
                            }
                        }
                        
                        ?>                        
                    </div>                                                           

                    <div id="controls" class="col-sm-12">
                    	<div class="inner">
                    		<p id="read-more" class="btn-green" data-tracking="Product|Read More|<?php echo get_the_title(); ?>">read more</p>
                    		<div class="arrows">
                    		    <img  href="#carousel-product" role="button" data-slide="prev" src="<?php echo get_template_directory_uri(); ?>/assets/images/ui/btn-arrow-prev.svg" alt="">
                    		    <img href="#carousel-product" role="button" data-slide="next" src="<?php echo get_template_directory_uri(); ?>/assets/images/ui/btn-arrow-next.svg" alt="">
                    		</div>
                    	</div>
                    </div>  
                </div>
				<!-- 
				***************************************
				ICI C'EST CACHÉ & SI ON CLIQUE SUR PLUS CA AFFICHE
				***************************************
				-->
				<div id="product-content">
					<div id="product-infos" class="col-sm-12">
						<p>The ECLYPSE Connected System Controller is a modular and scalable platform, allowing you to cost effectively address any HVAC application.</p>
						<p>Consisting of a control automation and connectivity server, power supply, and I/O modules, select the type and quantity of modules to cost effectively meet your exacting requirements.</p>
					</div>

					<div id="product-video">
						<iframe src="https://player.vimeo.com/video/116912711?title=0&byline=0&portrait=0" width="100%" height="280" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>
					</div>

					<div id="buzz" class="col-sm-12">
						<p class="title">What’s the buzz all about?</p>
						<blockquote>
							<p>Additional power supply, control, automation and connectivity servers, and I/O modules, allowing you to cost effectively address a wider range of applications.</p>
						</blockquote>
					</div>

					<div id="benefit" class="col-sm-12">
						<p class="title">Benefits</p>
						<ul>
							<li>A scaled aproach to cost effectively address project requirements</li>
							<li>Empowere IP connectivity</li>
							<li>Connect from anywhere using your mobile device</li>
						</ul>
						<div class="wrapper-btn">
							<a href="" class="btn-green">Get the specs & win</a>
						</div>
					</div>

					<div id="awards" class="col-sm-12">
						<p class="title">Awards</p>
						<ul>
							<li><img src="<?php echo get_template_directory_uri(); ?>/assets/images/new/award-1.jpg" alt=""></li>
							<li><img src="<?php echo get_template_directory_uri(); ?>/assets/images/new/award-2.jpg" alt=""></li>
							<li><img src="<?php echo get_template_directory_uri(); ?>/assets/images/new/award-3.jpg" alt=""></li>
							<li><img src="<?php echo get_template_directory_uri(); ?>/assets/images/new/award-4.jpg" alt=""></li>
						</ul>
					</div>

					<div id="features" class="col-sm-12">
						<p class="title">Features</p>
						<ul>
							<li>BTL listed as BACnet Building Controllers (B-BC)</li>
							<li>Simultaneous support of wired IP and Wi-Fi connectivity on a same controller</li>
							<li>Support Wi-Fi mesh </li>
							<li>Enables BACnet MS/TP and Modbus routing</li>
							<li>Proven high performance: accelerated processing, reading and control of up to 320 points</li>
							<li>Advanced, built-in security features and authentication service</li>
							<li>Hot-swappable tool-less design and unique latching mechanism</li>
						</ul>
					</div>

					<div id="related" class="col-sm-12">
						<p class="title">Software and Tools</p>
						<ul>
							<li>
								<a href="">
									<img src="<?php echo get_template_directory_uri(); ?>/assets/images/new/produit-thumb-1.jpg" alt="">
									<p class="name">Allure EC-Smart-Air</p>
								</a>
							</li>
							<li>
								<a href="">
									<img src="<?php echo get_template_directory_uri(); ?>/assets/images/new/produit-thumb.jpg" alt="">
									<p class="name">Allure EC-Smart-Air</p>
								</a>
							</li>
							<li>
								<a href="">
									<img src="<?php echo get_template_directory_uri(); ?>/assets/images/new/produit-thumb.jpg" alt="">
									<p class="name">Allure EC-Smart-Air</p>
								</a>
							</li>
							<li>
								<a href="">
									<img src="<?php echo get_template_directory_uri(); ?>/assets/images/new/produit-thumb-1.jpg" alt="">
									<p class="name">Allure EC-Smart-Air</p>
								</a>
							</li>
						</ul>
					</div>
				</div>

				<div class="wrapper-btn-up col-sm-12">
					<img id="btn-up" src="<?php echo get_template_directory_uri(); ?>/assets/images/ui/btn-arrow-up.svg" alt="">
				</div>

            </div>
        </div>
    </section>
<?php


$slug = isset($_POST["slug"])
?>