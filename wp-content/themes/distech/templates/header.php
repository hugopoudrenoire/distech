<div id="wrapper-spinner">
    <div class="spinner">
        <div class="bounce1"></div>
        <div class="bounce2"></div>
        <div class="bounce3"></div>
    </div>
</div>

<header>
    <div id="main-nav">
        <a id="main-logo" href="<?php echo esc_url(home_url('/')); ?>">
            <img src="<?php echo get_template_directory_uri(); ?>/assets/images/ui/main-logo.svg" alt="">
        </a>
        <span id="burger-menu"></span>
    </div>
    <div id="main-menu">
        <div class="outerCenter">
            <div class="middleCenter">
                <div class="innerCenter">
                    <nav class="nav-primary">
                        <?php
                        if (has_nav_menu('primary_navigation')) :
                          wp_nav_menu(['theme_location' => 'primary_navigation', 'menu_class' => 'nav']);
                        endif;
                        ?>
                    </nav>
                    <ul class="share-list">
                        <li><a href="https://twitter.com/DistControls_en" target="_blank"><img src="<?php echo get_template_directory_uri(); ?>/assets/images/ui/logo-tw.svg" alt="Twitter"></a></li>
                        <li><a href="https://www.facebook.com/distechcontrols" target="_blank"><img src="<?php echo get_template_directory_uri(); ?>/assets/images/ui/logo-fb.svg" alt="Facebook"></a></li>
                        <li><a href="https://www.youtube.com/user/distechcontrols" target="_blank"><img src="<?php echo get_template_directory_uri(); ?>/assets/images/ui/logo-yt.svg" alt="Youtube"></a></li>
                        <li><a href="https://www.linkedin.com/company/distech-controls" target="_blank"><img src="<?php echo get_template_directory_uri(); ?>/assets/images/ui/logo-in.svg" alt="Linkedin"></a></li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</header>