<head>
  <meta charset="utf-8">
  <meta http-equiv="x-ua-compatible" content="ie=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=no">
  <link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css">
  <?php wp_head(); ?>

  <script>
  	var templateDirectory = '<?php echo get_template_directory_uri(); ?>';
  </script>
    <script type="text/javascript">
      var ajaxurl = '<?php echo admin_url("admin-ajax.php"); ?>';
  </script>
  <script src="//f.vimeocdn.com/js/froogaloop2.min.js"></script>

</head>
