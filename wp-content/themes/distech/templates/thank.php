
	<!-- <section id="page-thank" class="wrapper-content popupThankyou">
	    <div class="container-fluid">
	        <div class="row">

	             <div id="section-1" class="col-sm-12 section">
	             	<div class="outerCenter">
	            		<div class="middleCenter">
	            			<div class="innerCenter">
				                <h1>Thank You</h1>
				                <p>How would you rate your experience at the Distech Controls booth?</p>
			                    <section id="sectionVote">
			                        <input id="input-star" type="number" class="rating" min=0 max=5 step=1 data-size="sm" >

			                        <button id="btnSubmitThank" class="btn-green">Submit</button>                        
			                        
			                    </section>
			                    <section id="sectionVoted" style="display:none;">
			                        <p>Voted !</p>
			                    </section>  
			                </div>
	            		</div>
	            	</div>                  

	            </div>
	        </div>
	    </div>
	</section> -->


	<div id="modal-thank" class="modal fade" tabindex="-1" role="dialog">
	    <div class="modal-dialog">
		    <div class="modal-content">
		        <div class="modal-header">
		        	<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
		        </div>
		        <div class="modal-body">
			        <div id="page-thank" class="wrapper-content popupThankyou">
			            <!-- <div id="section-1"> -->
			                <h1>Thank You</h1>
			                <p>How would you rate your experience at the Distech Controls booth?</p>
		                    <section id="sectionVote">
		                        <input id="input-star" type="number" class="rating" min=0 max=5 step=1 data-size="sm" >

		                        <button id="btnSubmitThank" class="btn-green">Submit</button>                        
		                        
		                    </section>
		                    <section id="sectionVoted" style="display:none;">
		                        <p>Voted !</p>
		                    </section>                   
			            <!-- </div> -->
					</div>
		        </div>
		    </div><!-- /.modal-content -->
	    </div><!-- /.modal-dialog -->
	</div><!-- /.modal -->